#version 330 core

uniform mat4 m_model;
uniform mat4 m_view;
uniform mat4 m_proj;
uniform int i_id;
uniform vec3 colorOverlay;

//uniform vec3 c_position;

in vec3 fragNormal;
in vec3 fragVertColor;

layout(location = 0) out vec3 diffuse;
layout(location = 1) out vec3 normal;
layout(location = 2) out vec3 material;
layout(location = 3) out vec3 id;

vec3 idToColor(int value)
{
   float red = value >> 16 & 0xFF;
   float green = value >> 8 & 0xFF;
   float blue = value & 0xFF;
   return vec3(red/255,green/255,blue/255);
}

void main()
{
   float average = (fragVertColor.x + fragVertColor.y + fragVertColor.z) * 0.333f;
   vec3 gray = vec3(average, average, average);
   float colorFactor = 0.25f;
   diffuse = fragVertColor * (1 - colorFactor) + colorFactor * (gray * colorOverlay);
   normal = fragNormal;
   material = vec3(0,0,0);
   id = idToColor(i_id);
}
