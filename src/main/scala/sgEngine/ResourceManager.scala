package sgEngine

import sgEngine.graphics._
import sgEngine.resources.{Mesh, Model, Texture, Material}
import sgEngine.util.{Matrix4, ProjectionFactory, ResourceUtil}
import scala.collection.mutable.Map
import scala.reflect.ClassTag

/**
 * Created by Toni on 17.1.2015.
 */
object ResourceManager {
  val width = 1280
  val height = 720


  val scaleFactor = 1  // For HDPI screens
  val ssFactor = 2  // Supersampling

  val textures = Map[String, Texture]()
  val meshes = Map[String,Mesh]()
  val shaderPrograms = Map[String, ShaderProgram]()
  val models = Map[String, Model]()
  val materials = Map[String, Material]()

  private lazy val keys = ResourceUtil.loadKeyBindings("keybindings.json")
  val keyBindings = keys.map(e => (e._1, InputKeys.keys(e._2))).toMap


  // global
  var windowID: Long = 0

  def init(wID: Long){
    windowID = wID

    shaderPrograms("default") =
      ResourceUtil.loadShader("shaders/default.vert","shaders/default.frag")

    shaderPrograms("passthrough") =
      ResourceUtil.loadShader("shaders/passthrough.vert", "shaders/passthrough.frag")

    shaderPrograms("grid") =
      ResourceUtil.loadShader("shaders/grid.vert", "shaders/grid.frag")
  }

  //textures("measure") = ResourceUtil.loadTexture("textures/measure.png")
  //textures("measure_128") = ResourceUtil.loadTexture("textures/measure_128.png")

  val load_model: PartialFunction[Component, Unit] = {
    case q: ModelComponent => {
      // TODO: Make sure to check if already loaded before loading. What if using textures?
      val model   = ResourceUtil.loadModel(q.model)
      val mesh     = ResourceUtil.loadMesh(model.mesh)
      val material = ResourceUtil.loadMaterial(model.material)
      models(q.model)           = model
      meshes(model.mesh)        = mesh
      materials(model.material) = material
    }
  }

  val load_else: PartialFunction[Component, Unit] = { case _ =>  }

  val loadComponentData = load_model orElse load_else

  /**
   * Loads map and all resources
   * @param path Path to map file
   */
  def loadMap(path: String): Vector[EntityData] = {
    val map = ResourceUtil.loadMap(path)

    // For each component in each entity, load component data
    map.foreach(e => {
      e.components.foreach(c => {
        loadComponentData(c)
      })
    })
    map
  }

  def dispose() = {
    textures.foreach(a => a._2.dispose())
    textures.clear()

    meshes.foreach(a => a._2.dispose())
    meshes.clear()

    shaderPrograms.foreach(a => a._2.dispose())
    shaderPrograms.clear()
  }

  lazy val projection = ProjectionFactory.createPerspectiveProjection(1000f,0.01f, width.toFloat/height.toFloat,45f)
}
