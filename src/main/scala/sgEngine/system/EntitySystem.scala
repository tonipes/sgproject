package sgEngine.systems

import sgEngine._
import scala.collection.immutable.BitSet
import scala.collection.mutable._

abstract class EntitySystem() {
  var world: World = null
  var compStruct: Vector[BitSet]
  protected var entities = Buffer[Int]()
  private var started = false

  /**
   * Processes all entities in systems entity list
   */
  def processEntities(delta: Float): Unit = {
    if(!started) {
      Log.error("Illegal system call", "System must be started before processing entities")
      throw new IllegalStateException("System must be started before processing entities")
    }
    else{
      for(entityID <- entities){
        process(entityID, delta)
      }
    }
  }

  /**
   * Checks if system is interested in entity by comparing component structure bitsets
   * If system is interested, entity will be added to systems entity list
   */
  def checkInterest(entityID: Int): Unit = {
    // if compStruct is empty, system is not interested in any entities
    if(!compStruct.isEmpty && this.isInterestedIn(entityID))
      addEntity(entityID)
  }

  /**
   * Removes entity from systems entity list if found
   */
  def removeEntity(entityID: Int): Unit  = {
    val index = entities.indexOf(entityID)
    if(index >= 0){
//      println(s"system destroy entity $entityID")
      entities.remove(index)
    }
  }

  /**
   * Adds entity to systems entity list
   */
  protected def addEntity(entityID: Int): Unit =
    entities += entityID

  /**
   * Checks if system is interested in entity
   */
  private def isInterestedIn(entityID: Int): Boolean = {
    this.compStruct.exists(s => (s & world.entities(entityID).compStruct) == s )
  }

  /**
   * Processes single entity
   */
  protected def process(entityID: Int, delta: Float): Unit

  final def begin(): Unit = {
    if (started) {
      Log.error("Illegal system call", "System must be ended before starting it again")
      throw new IllegalStateException("System must be ended before starting it again")
    } else {
      started = true
      systemBegin()
    }
  }

  final def end(): Unit = {
    if(!started){
      Log.error("Illegal system call", "System must be started before end")
      throw new IllegalStateException("System must be started before end")
    } else {
      started = false
      systemEnd()
    }
  }

  /**
   * Call this before processing entities
   */
  def systemBegin(): Unit = {}

  /**
   * Call this after processing entities
   */
  def systemEnd(): Unit = {}
}