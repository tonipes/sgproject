package sgEngine.util

/**
 * Created by Toni on 24.2.15.
 */
abstract class Behaviour {
  def update(delta: Float)

}
