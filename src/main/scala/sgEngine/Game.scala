package sgEngine

import sgEngine.system._

/**
 * Created by Toni on 17.1.2015.
 */
abstract class Game {

  def create(): Unit

  def render(): Unit

  def resize(width: Int, height: Int): Unit

  def update(delta: Float): Unit

  def dispose()
}
