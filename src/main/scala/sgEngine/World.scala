package sgEngine

import sgEngine.systems.EntitySystem
import scala.collection.mutable.{ArrayBuffer, Buffer, Map}

class World(entitySystems: EntitySystem*) {

  for(sys <- entitySystems){
    sys.world = this
  }

  val entities = Map[Int, Entity]()
  private val changed = ArrayBuffer[Int]()
  private val destroyed = ArrayBuffer[Int]()

  private val tags = Map[String, Int]()
  val groups = Map[String,Vector[Int]]().withDefault(a => Vector[Int]())

  /**
   * Updates world. Runs all systems
   */
  def update(delta: Float): Unit = {
    handleChanges()
    for(sys <- entitySystems){
      sys.begin()
      sys.processEntities(delta: Float)
      sys.end()
    }
  }

  /**
   * Checks if any systems are interested in changed entities
   */
  def handleChanges() = {
    for(system <- entitySystems){
      for(entID <- changed){  // Changed
        system.checkInterest(entID)
        entities(entID).changed = false
      }
      for(entID <- destroyed){ // Destroyed
        system.removeEntity(entID)
        entities.remove(entID)

      }
    }
    changed.clear()
    destroyed.clear()
  }

  /**
   * Adds entity to changed list. See handleChanges
   */
  def changed(entityID: Int): Unit = {
    entities.get(entityID) match {
      case Some(entity) if !entity.changed => {
        entity.changed = true
        changed += entityID
      }
      case None =>
    }
  }

  /**
   * Creates an entity from EntityData
   * @return created entity
   */
  var lastID = 0
  def createEntity(data: EntityData) = {
    // TODO: Create proper way of handling entity id's
    val id = lastID + 1
    val entity = new Entity(this, data.components,id)
    data.tags.foreach(t => addTag(t,id))
    entities(id) = entity
    changed += id
    lastID = id
    entity
  }

  def createEntities(data: Vector[EntityData]) =
    data.foreach(d => createEntity(d))

  /**
   * Adds tag to entity
   * @param tag tag to add
   */
  def addTag(tag: String, entityID: Int): Unit = tags(tag) = entityID

  /**
   * Returns entity with tag
   * @param tag tag to find
   * @return entity with tag or None
   */
  def getTagged(tag: String): Option[Int] = {
    if(tags.contains(tag))
      Some(tags(tag))
    else
      None
  }

  /**
   * Returns all tags that an entity has
   * @return all tags that given entity has
   */
  def getTags(entityID: Int): Vector[String] ={
    tags.filter(p => p._2 == entityID).map(p => p._1).toVector
  }

  /**
   * Destroys an entity
   */
  def destroyEntity(entityID: Int) = {
    entities.get(entityID) match {
      case Some(entity) => {
        destroyed += (entityID)
      }
      case None =>
    }
  }

  /**
   * Returns length of the entities list
   * @return length of entities list
   */
  def getEntityCount() = entities.size

  /**
   * Returns all EntityData from world.
   * @return all entityData
   */
  def getEntityData(): Vector[EntityData] = {
    entities.values.map(f => f.getEntityData).toVector
  }

  def dispose(): Unit = {
    // TODO: Write method
  }

}