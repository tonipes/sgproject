package sgEditor.systems

import org.lwjgl.opengl.GL13._
import sgEngine.resources.{Texture, Material, Model, Mesh}

import scala.collection.mutable.Buffer
import java.nio.FloatBuffer

import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL20._
import sgEngine._
import sgEngine.graphics.{Framebuffer, ShaderProgram}
import sgEngine.systems.EntitySystem
import sgEngine.util._

import scala.collection.immutable.BitSet
import scala.collection.mutable.Map

/**
 * Created by Toni on 17.1.2015.
 */

class EditorRenderSystem(
                          framebuffer: Framebuffer,
                          meshes: Map[String,Mesh],
                          shaderPrograms: Map[String, ShaderProgram],
                          models: Map[String, Model],
                          materials: Map[String, Material],
                          textures: Map[String, Texture]
                          ) extends EntitySystem {

  override var compStruct = Vector(
      BitSet.empty +
      Component.getID(classOf[SpatialComponent]) +
      Component.getID(classOf[ModelComponent]),
      BitSet.empty +
      Component.getID(classOf[SpatialComponent]) +
      Component.getID(classOf[LightSourceComponent])
  )

  val batchedEntities = Map[ModelComponent, Buffer[Entity]]()
  val lights = Buffer[(SpatialComponent, LightSourceComponent)]()
  val selectionColor = Vec4(1.0f,0f,0f,1f)

  // GL setup
  //glClearColor(0.15f, 0.15f, 0.15f, 1.0f);
  glEnable(GL_DEPTH_TEST)
  glEnable(GL_CULL_FACE)
  glEnable(GL_TEXTURE_2D)

  glLineWidth(1f * ResourceManager.ssFactor)
  glEnable(GL_LINE_SMOOTH)
  glHint(GL_LINE_SMOOTH_HINT,  GL_NICEST)

  lazy val defaultShader: ShaderProgram = shaderPrograms("default")
  lazy val passthroughShader: ShaderProgram = shaderPrograms("passthrough")
  lazy val gridShader: ShaderProgram = shaderPrograms("grid")

  //lazy val framebuffer = new sgEngine.graphics.Framebuffer(size,size,"diffuse","normal", "material", "id")
  lazy val screenPlane = MeshFactory.createScreenMesh()

  val grid = MeshFactory.createGridMesh(8, 8, 16, 16)
  val manipulator = MeshFactory.createTransformManipulator(5)

  override def addEntity(entityID: Int) = {
    // TODO: Should get bitset that matches => match
    world.entities.get(entityID) match {
      case Some(ent) => {
        if(ent.containsComponent(classOf[ModelComponent])){ // Add to entity list
          // TODO: Why is entity in batchedEntities. Isn't spatial enough
          val model = ent.getComponent(Component.model).get
          if(batchedEntities.contains(model)){
            batchedEntities(model) ++= Vector[Entity](ent)
          }
          else{
            batchedEntities(model) = Buffer[Entity](ent)
          }
        }
        if(ent.containsComponent(classOf[LightSourceComponent])){ // Add to light list
          val light = ent.getComponent(Component.light).get
          val spat = ent.getComponent(Component.spatial).get
          lights += Tuple2(spat, light)
        }
      }
      case None =>
    }
  }

  override def removeEntity(entityID: Int) = {
    batchedEntities.foreach(e => {
      val index = e._2.indexWhere(e => e.id == entityID)
      if(index >= 0) {
        e._2.remove(index)
      }
    })
    val lightIndex = lights.indexOf(entityID)
    if(lightIndex >= 0) {
      lights.remove(lightIndex)
    }
  }

  override def systemBegin(): Unit = {

  }

  override protected def process(entityID: Int, delta: Float): Unit = {

  }

  /*
   * Renders scene
   */
  override def systemEnd(): Unit = {
    if(!world.getTagged("camera").isDefined) {
      Log.error("No camera found!")
      throw new IllegalStateException("No camera found!")
    }

    val camera = world.entities(world.getTagged("camera").get)

    // Camera is needed
    val cameraSpat = camera.getComponent(Component.spatial).get
    val cameraPos = cameraSpat.position

    val viewBuffer       = getViewBuffer(cameraSpat.position,cameraSpat.orientation)
    val projectionBuffer = BufferFactory.createFloatBuffer(ResourceManager.projection)

    // Get ready to draw scene to framebuffer
    glViewport(0,0,ResourceManager.width * ResourceManager.ssFactor,ResourceManager.height * ResourceManager.ssFactor)
    framebuffer.bindFramebuffer()
      glDrawBuffers(BufferFactory.createIntBuffer(framebuffer.attachments))
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
      renderScene(viewBuffer, projectionBuffer)
      renderEditorHelpers(viewBuffer, projectionBuffer, cameraPos)
    framebuffer.unbind()

    // Render framebuffer to screen
    glViewport(0,0,ResourceManager.width * ResourceManager.scaleFactor,ResourceManager.height * ResourceManager.scaleFactor)
    passthroughShader.bind()
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
      renderToScreen()
    passthroughShader.unbind()

  }

  /*
   * Renders grid and manipulator
   */
  private def renderEditorHelpers(viewBuffer: FloatBuffer, projectionBuffer: FloatBuffer, cameraPos: Vec3) = {
    val spat = SpatialComponent(Vec3(0,0,0),Quaternion(0,0,0,1),Vec3(1,1,1))

    val manipulatorLoc = getMedianPoint(
      world.groups("selection").map(e =>
        world.entities(e).getComponent(Component.spatial).get.position
      ))

    gridShader.bind()
    gridShader.setUniform("m_model", getFloatBuffer(spat.position ,spat.orientation, spat.scale))
    gridShader.setUniform("m_view", viewBuffer)
    gridShader.setUniform("m_proj", projectionBuffer)

    grid.bindVAO()
    grid.bindElem()

    glEnableVertexAttribArray(0)
    glEnableVertexAttribArray(1)

    glDrawElements(GL_LINES, grid.elemCount, GL_UNSIGNED_INT, 0)

    glDisableVertexAttribArray(0)
    glDisableVertexAttribArray(1)

    grid.unbindVAO()
    grid.unbindElem()

    if(manipulatorLoc.isDefined){
      //println(manipulatorLoc.get)
      val distanceToCamera = getDistance(manipulatorLoc.get, cameraPos) * 0.05f
      gridShader.setUniform("m_model",
        getFloatBuffer(
          manipulatorLoc.get,
          spat.orientation,
          Vec3(distanceToCamera,distanceToCamera,distanceToCamera)))

      manipulator.bindVAO()
      manipulator.bindElem()

      glEnableVertexAttribArray(0)
      glEnableVertexAttribArray(1)

      glDrawElements(GL_LINES, manipulator.elemCount, GL_UNSIGNED_INT, 0)

      glDisableVertexAttribArray(0)
      glDisableVertexAttribArray(1)

      manipulator.unbindVAO()
      manipulator.unbindElem()

    }
    gridShader.unbind()

  }

  /*
   * Renders all entities
   */
  private def renderScene(viewBuffer: FloatBuffer, projectionBuffer: FloatBuffer) = {
    defaultShader.bind()
    defaultShader.setUniform("m_view", viewBuffer)
    defaultShader.setUniform("m_proj", projectionBuffer)

    for(i <- batchedEntities){
      val model = models(i._1.model)
      val mesh = meshes(model.mesh)

      mesh.bindVAO()
      mesh.bindElem()

      glEnableVertexAttribArray(0)
      glEnableVertexAttribArray(1)
      glEnableVertexAttribArray(2)
      glEnableVertexAttribArray(3)

      for(e <- i._2){
        val spat = e.getComponent(Component.spatial).get
        defaultShader.setUniform("m_model", getFloatBuffer(spat.position, spat.orientation, spat.scale))

        defaultShader.setUniformInt("i_id", e.id)

        if(world.groups("selection").contains(e.id)) {
          defaultShader.setUniform("colorOverlay", selectionColor.x,selectionColor.y,selectionColor.z)
        }
        else{
          defaultShader.setUniform("colorOverlay", 0,0,0)
        }

        glDrawElements(GL_TRIANGLES,mesh.elemCount,GL_UNSIGNED_INT,0)
      }
      glDisableVertexAttribArray(0)
      glDisableVertexAttribArray(1)
      glDisableVertexAttribArray(2)
      glDisableVertexAttribArray(3)

      mesh.unbindVAO()
      mesh.unbindElem()
    }
    defaultShader.unbind()
  }

  /*
   * Renders framebuffer to screen.
   */
  private def renderToScreen() = {
    framebuffer.activateTextures(passthroughShader)

    screenPlane.bindVAO()
    screenPlane.bindElem()

    glEnableVertexAttribArray(0)
    glEnableVertexAttribArray(1)

    glDrawElements(GL_TRIANGLES,screenPlane.elemCount,GL_UNSIGNED_INT,0)

    glDisableVertexAttribArray(0)
    glDisableVertexAttribArray(1)

    screenPlane.unbindVAO()
    screenPlane.unbindElem()

  }

  /*
   * Makes floatbuffer from spatial data to load it to shader
   */
  private def getFloatBuffer(position: Vec3, orientation: Quaternion, scaleVec: Vec3): FloatBuffer ={
    val scale  = Matrix4.getScale(scaleVec)
    val trans  = Matrix4.getTransformation(position)
    val rotate = orientation.rotationMatrix()
    val mul    = trans * rotate * scale

    BufferFactory.createFloatBuffer( mul.transpose )
  }

  private def getViewBuffer(position: Vec3, orientation: Quaternion): FloatBuffer = {
    val scale  = Matrix4.getScale(Vec3(1,1,1))
    val trans  = Matrix4.getTransformation(position.neg)
    val rotate = orientation.getConjugate().rotationMatrix()
    val mul    = scale * rotate * trans

    BufferFactory.createFloatBuffer( mul.transpose )
  }

  private def getMedianPoint(in: Vector[Vec3]): Option[Vec3] = {
    if(in.length != 0)
      Some(in.fold(Vec3(0,0,0))((a,b) => a+b) * (1/in.length.toFloat))
    else
      None
  }

  private def getDistance(a: Vec3, b: Vec3): Float = {
    val x = a.x - b.x
    val y = a.y - b.y
    val z = a.z - b.z
    math.sqrt(x*x + y*y + z*z).toFloat
  }

}
