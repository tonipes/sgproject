package sgEditor.systems

import org.lwjgl.BufferUtils
import org.lwjgl.glfw.GLFW._
import sgEngine.{InputKeys, ResourceManager, Input}
import sgEngine.graphics.Framebuffer
import sgEngine.system.LogicSystem
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL30._

/**
 * Created by Toni on 4.3.2015.
 */
class EditorSelectionSystem(windowID: Long, keyMap: Map[String,(Int,Int)], framebuffer: Framebuffer) extends LogicSystem {
  val edit_selection_mod = keyMap("edit_selection_mod")
  val edit_delete_selection = keyMap("edit_action_delete")

  override def processSystem(delta: Float): Unit = {
    if (Input.isMouseJustPressed(GLFW_MOUSE_BUTTON_1)) {

      val pixelBuffer = BufferUtils.createByteBuffer(3)
      val mouseLoc = Input.mouseCursorPos

      val x = mouseLoc.x.toInt * ResourceManager.ssFactor
      val y = (framebuffer.height - mouseLoc.y * ResourceManager.ssFactor).toInt

      // Read pixel color data from framebuffer attachment with name "id" at (x,y)
      glBindFramebuffer(GL_READ_FRAMEBUFFER, framebuffer.framebufferID)
      glReadBuffer(framebuffer.attachments(framebuffer.textures.indexOf("id")))
      glReadPixels(x, y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixelBuffer)
      glReadBuffer(GL_NONE)
      glBindFramebuffer(GL_READ_FRAMEBUFFER, 0)

      val id = getIDfromColor(pixelBuffer.get(0), pixelBuffer.get(1), pixelBuffer.get(2))

//      println(s"Selection ${pixelBuffer.get(2)}, $x:$y")

      // selection managment
      id match {
        case 0 if Input.isPressed(edit_selection_mod) => {}

        case 0 if !Input.isPressed(edit_selection_mod) =>
          world.groups("selection") = Vector()

        case q if Input.isPressed(edit_selection_mod) => {
          if(!world.groups("selection").contains(q))
            world.groups("selection") = world.groups("selection") ++ Vector(id)
          else
            world.groups("selection") =
              world.groups("selection") patch (from = world.groups("selection").indexOf(q), patch = Nil, replaced = 1)
        }
        case q if !Input.isPressed(edit_selection_mod) =>
          world.groups("selection") = Vector(id)
      }
    }

//    if(Input.isJustPressed(GLFW_KEY_Z)){
//      printAllIds(world.groups("selection"))
//    }

    if(Input.isJustPressed(edit_delete_selection)){
      world.groups("selection").foreach(e => world.destroyEntity(e))
      world.groups("selection") = Vector()
    }
  }

  private def getIDfromColor(r: Byte, g: Byte, b: Byte): Int = {
    val bR = (r << 16) & 0xFF0000
    val bG = (g << 8) & 0x00FF00
    val bB = b & 0x0000FF

    return bR | bG | bB
  }

//  private def printAllIds(a: Vector[Int]) = {
//    println("SELECTION")
//    a.foreach(e => {
//      val entity = world.entities(e)
//      println(s"\t\tTags: ${entity.getTags}, ID: ${entity.id}")
//    })
//    println("SELECTION END")
//  }



}
