package sgEditor.systems

import org.lwjgl.glfw.GLFW._
import sgEngine._
import sgEngine.system.LogicSystem
import sgEngine.systems.EntitySystem
import sgEngine.util.{Vec2, Vec4, Quaternion, Vec3}

import scala.collection.immutable.BitSet

/**
 * Created by Toni on 5.2.2015.
 */

class EditorInputSystem(keyMap: Map[String,(Int,Int)]) extends LogicSystem {
  val targetCamStruct = BitSet.empty
    Component.getID(classOf[SpatialComponent]) +
    Component.getID(classOf[TargetCameraComponent])

  private val cam_btn      = keyMap("cam_transform_button")
  private val cam_mod_pan  = keyMap("cam_transform_mod_pan")
  private val cam_mod_zoom = keyMap("cam_transform_mod_zoom")

  private val cam_fac_pan        = 0.01f
  private val cam_fac_zoom       = 0.02f
  private val cam_fac_scrollZoom = 10f
  private val cam_fac_rotate     = 0.4f

  // All mouse camera movement is relative to these values.
  private var zoomBase: Option[Float] = None
  private var panBase: Option[Vec3] = None
  private var rotateBase: Option[Quaternion] = None

  def baseDefined = zoomBase.isDefined || panBase.isDefined || rotateBase.isDefined

  override def processSystem(delta: Float): Unit = {
    val cameraID = world.getTagged("camera")

    cameraID match {
      case Some(q) =>
        world.entities.get(q) match {
          case Some(e) if matches(e.compStruct, targetCamStruct) =>
            handleTargetCamera(e.getComponent(Component.spatial).get, e.getComponent(Component.targetCam).get)
          case None => Log.error("No suitable camera found!")
        }
      case None => Log.error("No suitable camera found!")
    }
  }

  private def handleTargetCamera(spat: SpatialComponent, targetCam: TargetCameraComponent): Unit = {
//    if (Input.isJustPressed(GLFW_KEY_SPACE))
//      println(s"Camera lookAt ${targetCam.lookAt}, Pos:${spat.position}, Quat: ${spat.orientation}")

    // Main mouse movement
    if (Input.isPressed(cam_btn)){
      val mouseDrag = Input.mouseDrag(cam_btn._2)
      if(Input.isPressed(cam_mod_zoom) || zoomBase.isDefined)
        zoom(targetCam,mouseDrag)
      else if(Input.isPressed(cam_mod_pan) || panBase.isDefined)
        pan(spat, targetCam, mouseDrag)
      else
        rotate(spat, targetCam.lookAt, mouseDrag)
    }
    else{
      zoomBase = None
      panBase = None
      rotateBase = None
    }

    // Scroll Zoom
    targetCam.distance =  targetCam.distance * (1 - (Input.scrollState.y * cam_fac_zoom * cam_fac_scrollZoom))
    spat.position = targetCam.lookAt.copy()
    spat.move(spat.forward(), targetCam.distance)
  }

  private def rotate(spat: SpatialComponent, lookAt: Vec3, mov: Vec2) = {
    if(rotateBase.isDefined) {
      spat.orientation = Quaternion.fromAxisAngle(Vec3(0, 1, 0), -cam_fac_rotate * mov.x) * rotateBase.get
      spat.orientation = Quaternion.fromAxisAngle(spat.right(), -cam_fac_rotate * mov.y) * spat.orientation
    }
    else {
      rotateBase = Some(spat.orientation)
    }
  }

  private def pan(spat: SpatialComponent, targetCam: TargetCameraComponent, mov: Vec2) = {
    if(panBase.isDefined){
      val up = spat.up().normalise()
      val right = spat.right().normalise()

      targetCam.lookAt = panBase.get + up * cam_fac_pan * mov.y
      targetCam.lookAt = targetCam.lookAt + right * -cam_fac_pan * mov.x
    }
    else{
      panBase = Some(targetCam.lookAt)
    }
  }

  private def zoom(targetCam: TargetCameraComponent, mov: Vec2) = {
    if(zoomBase.isDefined)
      targetCam.distance = math.max(zoomBase.get + mov.y * cam_fac_zoom, 0f)
    else
      zoomBase = Some(targetCam.distance)
  }

  private def matches(a: BitSet, b: BitSet): Boolean =
    (a & b) == b
}


