package sgEditor

import org.lwjgl.opengl.GL11._
import sgEditor.systems.{EditorSelectionSystem, EditorRenderSystem, EditorInputSystem}
import sgEngine.graphics.Framebuffer
import sgEngine.util.Vec3
import sgEngine.{ResourceManager, World, Game}

/**
 * Created by Toni on 15.2.15.
 */
class EditorGame extends Game {

  lazy val framebuffer = new Framebuffer(
    ResourceManager.width * ResourceManager.ssFactor,
    ResourceManager.height * ResourceManager.ssFactor,"diffuse","normal", "material", "id")

  lazy val world: World = new World(
    new EditorSelectionSystem( ResourceManager.windowID, ResourceManager.keyBindings, framebuffer),
    new EditorInputSystem(ResourceManager.keyBindings),
    new EditorRenderSystem(
      framebuffer,
      ResourceManager.meshes,
      ResourceManager.shaderPrograms,
      ResourceManager.models,
      ResourceManager.materials,
      ResourceManager.textures
    ))

  lazy val mapData = ResourceManager.loadMap("maps/default.map")
  lazy val editorSettings = ResourceManager.loadMap("editor_settings.map")

  override def create(): Unit = {
    world.createEntities(editorSettings)
    world.createEntities(mapData)
  }

  override def resize(width: Int, height: Int): Unit = {
    ???
  }

  override def update(delta: Float): Unit = {
    world.update(delta)
  }

  override def dispose(): Unit = {
    world.dispose()
  }

  override def render(): Unit = {
    ???
  }
}
