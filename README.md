## README ##
Entity-component-system design 3D game engine and level editor made using scala and lwjgl.

Runnable version can be found [here](https://www.dropbox.com/s/2wx2d633v60tw4z/sgEditor.zip?dl=0)

## Keybindings ##
* Camera rotation - mouse_right
* Camera movement - mouse_right + left_shift
* Camera zoom - mouse_right + left_ctrl
* Select object - mouse_left
* Select another - left_shift + mouse_left
* Deselect all / select all - a
* Delete selection - x
* Move selection in level - g
* Move selection vertically - mouse roll
* Duplicate selection - left_shift + d
* Add object (.model file) - left_shift + a
* Model files can be found in resources/models/ 

More info about keybindings in keybindins.json file